var Product = require('../models/productModel.js');
var mongoose = require('mongoose');
var dbCfg = require('../config/database');
mongoose.Promise = global.Promise;
mongoose.connect(dbCfg.url);

var products = [
    new Product({
        name : 'Hamburger 1',
        excerpt : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. ',
        content : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu s,',
        img : '/images/products/1.jpg',
        category : 'food',
        price : 10.23,
        createdAt :  new Date().toISOString().slice(0, 19).replace('T', ' '),
    }),
    new Product({
        name : 'Hamburger 2',
        excerpt : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. ',
        content : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu s,',
        img : '/images/products/2.jpg',
        category : 'food',
        price : 10.53,
        createdAt :  new Date().toISOString().slice(0, 19).replace('T', ' '),
    }),
    new Product({
        name : 'Hamburger 3',
        excerpt : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. ',
        content : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu s,',
        img : '/images/products/3.jpg',
        category : 'food',
        price : 12.99,
        createdAt :  new Date().toISOString().slice(0, 19).replace('T', ' '),
    }),
    new Product({
        name : 'Hamburger 4',
        excerpt : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. ',
        content : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu s,',
        img : '/images/products/4.jpg',
        category : 'food',
        price : 13.88,
        createdAt :  new Date().toISOString().slice(0, 19).replace('T', ' '),
    }),
    new Product({
        name : 'Hamburger 5',
        excerpt : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. ',
        content : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu s,',
        img : '/images/products/5.jpg',
        category : 'food',
        price : 44.25,
        createdAt :  new Date().toISOString().slice(0, 19).replace('T', ' '),
    }),

    new Product({
        name : 'Hamburger 6',
        excerpt : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. ',
        content : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu s,',
        img : '/images/products/6.jpg',
        category : 'food',
        price : 18.13,
        createdAt :  new Date().toISOString().slice(0, 19).replace('T', ' '),
    }),


    new Product({
        name : 'Hamburger 7',
        excerpt : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. ',
        content : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu s,',
        img : '/images/products/6.jpg',
        category : 'food',
        price : 18.13,
        createdAt :  new Date().toISOString().slice(0, 19).replace('T', ' '),
    }),


    new Product({
        name : 'Hamburger 8',
        excerpt : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. ',
        content : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu s,',
        img : '/images/products/6.jpg',
        category : 'food',
        price : 1.13,
        createdAt :  new Date().toISOString().slice(0, 19).replace('T', ' '),
    }),


    new Product({
        name : 'Hamburger 9',
        excerpt : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. ',
        content : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu s,',
        img : '/images/products/6.jpg',
        category : 'food',
        price : 3.15,
        createdAt :  new Date().toISOString().slice(0, 19).replace('T', ' '),
    }),


    new Product({
        name : 'Hamburger 10',
        excerpt : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. ',
        content : 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu s,',
        img : '/images/products/6.jpg',
        category : 'food',
        price : 7.17,
        createdAt :  new Date().toISOString().slice(0, 19).replace('T', ' '),
    }),
];

var counter = 0;
for (var i = 0; i < products.length; i++) {
    products[i].save(function(err, result){
        console.log(err);
        counter++;

        if(counter === products.length) {
            mongoose.disconnect();
        }
    });

}

