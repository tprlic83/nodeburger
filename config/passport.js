
var LocalStrategy   = require('passport-local').Strategy;
var User            = require('../models/userModel');


module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });


    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true
        },
        function(req, email, password, done) {

            // asynchronous
            process.nextTick(function() {


                User.findOne({ 'email' :  email }, function(err, user) {

                    if (err)
                        return done(err);

                    if (user) {
                        return done(null, false, req.flash('message', 'That email is already taken.'));
                    } else {

                        var newUser  = new User();

                        newUser.email    = email;
                        newUser.password = newUser.generateHash(password);
                        newUser.admin = false;
                        newUser.avatar = '/images/avatar.png';
                        newUser.createAt = new Date().toISOString().slice(0, 19).replace('T', ' ');

                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }

                });

            });

        }));


    passport.use('local-login', new LocalStrategy({

            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true
        },
        function(req, email, password, done) {

            User.findOne({ 'email' :  email }, function(err, user) {

                if (err)
                    return done(err);

                if (!user)
                    return done(null, false, req.flash('message', 'Wrong username'));

                if (!user.validatePassword(password))
                    return done(null, false, req.flash('message', 'Wrong password'));

                return done(null, user);
            });

        }));

};