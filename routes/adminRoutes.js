var express = require('express');
var router = express.Router();

var Product = require('../models/productModel');
var User = require('../models/userModel');
/*
 * GET
 */
router.get('/', function(req,res,next){

    Product.find(function (err, products) {
        if (err) {
            //TODO handle error
        }

        products = products;

        User.find(function (err, users) {
            if (err) {
                //TODO handle error
            }
            res.render('admin', {title: 'Admin', products: products, users: users});
        })
    });
});

router.get('/product', function( req, res){
    res.render('../views/product/add', {title: 'Admin | Product Add'});
});

router.get('/product/:id', function( req,res) {
    var id = req.params.id;
    Product.findOne({_id: id},(function (err, product) {
        if (err) {
            //TODO handle error
        }
        console.log(product);
        res.render('../views/product/update', {title: 'Admin | Product edit', product: product});
    }));
});



module.exports = router;
