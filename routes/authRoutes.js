var isLoggedIn = require('../middleware/isLoggedIn');


module.exports = function(app, passport) {

    app.get('/auth', function(req, res) {
        res.render('index');
    });

    //Handle Signup
    app.post('/auth/signup', passport.authenticate('local-signup', {
        successRedirect : '/',
        failureRedirect : '/signup',
        failureFlash : true
    }));

    //Handle Login
    app.post('/auth/login', passport.authenticate('local-login', {
        successRedirect : '/',
        failureRedirect : '/',
        failureFlash : true
    }));


    app.get('/auth/signup', function(req, res) {
        res.render('signup', { message: req.flash('message') });
    });



    app.get('/auth/logout', isLoggedIn, function(req, res) {
        req.logout();
        res.locals.session.admin = false;
        res.redirect('/');
    });
};