var express = require('express');
var router = express.Router();
var productController = require('../controllers/productController.js');
var crypto = require('crypto');
var mime = require('mime');

var isAdmin = require('../middleware/isAdmin');

var multer  = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/images/products//')
    },
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
        });
    }
});
var upload = multer({ storage: storage });


/*
* GET
*/
router.get('/:name/:id', productController.show);

/*
 * GET
 */
router.get('/', productController.list);

/*
 * GET
 */
router.get('/latest', productController.latest);


/*
 * POST
 */
router.post('/', isAdmin, upload.single('img'), productController.create);

/*
 * PUT
 */
router.put('/:id', isAdmin, upload.single('img'), productController.update);

/*
 * DELETE
 */
router.delete('/:id', isAdmin, productController.remove);

module.exports = router;
