var express = require('express');
var router = express.Router();
var Product = require('../models/productModel');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Home' });
});

module.exports = router;
