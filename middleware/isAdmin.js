module.exports = function isAdmin(req, res, next) {
    if(req.user && req.user.admin){
        next();
    }else {
        res.send(401, 'Unauthorized');
    }

};