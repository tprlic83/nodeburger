var productModel = require('../models/productModel.js');
var mime = require('mime');

module.exports = {

    /**
     * productController.list()
     */
    list: function (req, res) {
        productModel.find(function (err, products) {
            //TODO error check

            if(req.xhr){
                return res.json(products);
            }

            res.render('product', {title: 'Products', products: products});
        }, {description: 0}).sort({ _id: -1});
    },

    /**
     * productController.latest()
     */
    latest: function (req, res) {
        productModel.find(function (err, products) {
            //TODO error check
            return res.json(products);
        }, {description: 0}).limit(6).sort({ _id: -1 });
    },

    /**
     * productController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        productModel.findOne({_id: id}, function (err, product) {
            //TODO error check
            //Check if request is ajax
            if(req.xhr){
                return res.json(product);
            }

            res.render('../views/product/single', {title: product.name, product: product});
        });
    },

    /**
     * productController.create()
     */
    create: function (req, res, file) {


        var product = new productModel({
			name : req.body.name,
			excerpt : req.body.excerpt,
			content : req.body.content,
			img : '/images/products/' + req.file.filename,
			category : req.body.category,
			price : req.body.price,
			createdAt : req.body.createdAt,
			updatedAt : req.body.updatedAt
        });

        product.save(function (err, product) {
            if (err) {
                req.flash('error', err);
                res.redirect('/admin/product');
            }

            req.flash('success', true);
            res.redirect('/admin');
        });
    },

    /**
     * productController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        productModel.findOne({_id: id}, function (err, product) {
            if (err) {
                req.flash('error', err);
                res.redirect('/admin/product');
            }

            if (!product) {
                req.flash('error', err);
                res.redirect('/admin/product');
            }

            product.name = req.body.name ? req.body.name : product.name;
			product.excerpt = req.body.excerpt ? req.body.excerpt : product.excerpt;
			product.content = req.body.content ? req.body.content : product.content;
			product.img = req.file ? '/images/products/' + req.file.filename : product.img;
			product.category = req.body.category ? req.body.category : product.category;
			product.price = req.body.price ? req.body.price : product.price;
			product.createdAt = req.body.createdAt ? req.body.createdAt : product.createdAt;
			product.updatedAt = req.body.updatedAt ? req.body.updatedAt : product.updatedAt;
			
            product.save(function (err, product) {
                if (err) {
                    req.flash('error', err);
                    res.redirect('/admin/product');
                }

                req.flash('success', true);
                res.redirect('/admin');
            });
        });
    },

    /**
     * productController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        productModel.findByIdAndRemove(id, function (err, product) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the product.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
